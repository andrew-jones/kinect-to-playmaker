﻿using HutongGames.PlayMaker;
using UnityEngine;
using System.Collections;
using System;
using System.IO;

/*
 * This playmaker script gets the current frame of the Kinect feed and saves it as 
 * a texture. The texture is then saved as a file if the user wants.
 */
namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Kinect SDK")]//Adds the script to the current category in playmaker actions, or adds the category if it does not exist
	[Tooltip("Allows you to get the colour map from the kinect and save it as a texture. Can be used for a snapshot effect. NOTE That you must make sure compute colour map is enabled under KinectManager Script.")]//The tooltip to appear when hovering over the action
	
	public class KinectSnapShot : FsmStateAction//Extends FsmStateAction to be a playmaker script
	{
		[CheckForComponent(typeof(KinectManager))]//Check that the GameObject has the KinectManager component
		[Tooltip("The GameObject to get KinectManager from.")]//Tooltip to display when hovering over the variable
		public FsmOwnerDefault kinectManager;//Holds the GameObject that contains the Kinect manager
		
		[UIHint(UIHint.Variable)]//Display what type of variable the user should pass in
		[Tooltip("Stores the colour map from the Kinect.")]//Tooltip to display when hovering over the variable
		public FsmTexture storeResult;//Texture that will store the colour map from Kinect

		[Tooltip("Stores the colour map from the Kinect.")]//Tooltip to display when hovering over the variable
		public FsmString nameOfImage;//Name the image should be saved as

		[Tooltip("Whether to save the texture to a file or not.")]//Tooltip to display when hovering over the variable
		public FsmBool saveFile = true;//Holds the value entered by the user

		public FsmString pathFromAssets;

		private KinectManager manager;//Holds the KinectManager from kinectManager passed in by user
		private bool save;//Holds the value from saveFile

		//when the script is first run
		public override void OnEnter()
		{			
			manager = kinectManager.GameObject.Value.gameObject.GetComponent<KinectManager>();//Retrieve the KinectManager component

			save = saveFile.Value;

			if(!(pathFromAssets.Value.StartsWith("/")))//If the users input for path does not start with a /
				pathFromAssets.Value = "/" +pathFromAssets.Value;//Add it on to the start

			if(!(pathFromAssets.Value.EndsWith("/")))//If the users input for path does not end with a /
				pathFromAssets.Value = pathFromAssets.Value + "/";//Add it on to the end

			colourMap();
		}

		/*
		 * This method gets the colour map from the Kinect, converts it to a material,
		 * then if the player wishes to save the texture to a file that is done so.
		 * The method finally ends by calling the Finish event, as there is no option
		 * to continually update.
		 */
		private void colourMap()
		{	
			if(manager != null && KinectManager.IsKinectInitialized())
			{
				Texture2D tex2d = manager.GetUsersClrTex();//Get the colour map
				Texture2D newTexture = new Texture2D(tex2d.width, tex2d.height, TextureFormat.ARGB32, false);//Create a new texture that will hold the pixels
				
				newTexture.SetPixels(0,0, tex2d.width, tex2d.height, tex2d.GetPixels());//'Get the current frame' and set it to newTexture
				newTexture.Apply();//Apply the SetPixels change

				storeResult.Value = newTexture;//Store the result as a new Texture

				if(save)//If the user wants to save the image
					SaveTextureToFile(newTexture, nameOfImage.Value);//Save it
			}

			Finish ();//Send the Finish event
		}

		/*
		 * This method saves the texture passed in at a set directory,
		 * under the file name that was passed in.
		 * 
		 * texture is the texture that should be saved.
		 * fileName is the name of the file that should be saved.
		 */
		private void SaveTextureToFile(Texture2D texture, String fileName)
		{
			byte[] bytes = texture.EncodeToPNG();//Convert the texture to bytes.
			string originalFileName = fileName;//Store the original value

			if(!(Directory.Exists(Application.dataPath + pathFromAssets)))//If directory does not exist
				Directory.CreateDirectory(Application.dataPath + pathFromAssets);//Create the directory

			if(File.Exists(Application.dataPath + pathFromAssets + fileName + ".png"))//Perform a check here to see if the file exists
			{
				int count = 1;//Used to add fileName(i)
				do
				{
					fileName =  originalFileName + "(" + count++ + ")";//put a (count) around what the user wanted
				}while(File.Exists(Application.dataPath + pathFromAssets + fileName + ".png"));//Continue to do until file does not exist
			}
	
			File.WriteAllBytes(Application.dataPath + pathFromAssets + fileName + ".png", bytes);//Write the file
			Debug.Log("KinectSnapShot.cs - Save Picture: " + (Application.dataPath + pathFromAssets + fileName + ".png"));//Log it as being saved
			//Tell unity to delete the texture, by default it seems to keep hold of it and memory crashes will occur after too many screenshots.
		}
	}//End of class
}//End of namespace